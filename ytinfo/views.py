from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from . import data

@csrf_exempt
def main(request):
    if request.method=="POST":
        print(request.POST)
        id=request.POST['id']
        for video in data.videos:
            if video['id'] == id:
                if request.POST['action'] == "Select":
                    video['selected'] = True
                elif request.POST['action'] == "Deselect":
                    video['selected'] = False
                break

    content_list = data.videos
    context = {'lista_videos': content_list}
    return render(request, 'ytinfo/main.html', context)

@csrf_exempt
def ytvideo(request, id):
    v = {}
    content_list = data.videos
    for video in data.videos:
        if video['id'] == id:
            v = video
            break
    cinfo = data.channel
    context = {'video': v,
                'cinfo': cinfo}
    return render(request, 'ytinfo/ytvideo.html', context)
