from django.urls import path

from . import views

urlpatterns = [
    path('', views.main),
    path('<str:id>',views.ytvideo)
]
