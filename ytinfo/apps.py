import urllib.request

from django.apps import AppConfig
from .ytparser import YTParser
from . import data

class YTInfoConfig(AppConfig):
    name = 'ytinfo'
    def ready(self):
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        yt = YTParser(xmlStream)
        data.videos = yt.videos()
        for video in data.videos:
            video['selected'] = False
        data.channel = yt.channel()
